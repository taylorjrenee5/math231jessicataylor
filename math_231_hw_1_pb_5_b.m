%% Math 231_Homework_1_problem_5_part_b.m
clear all
close all
clc
format long
%% Jessica Taylor
%-------------------------------------------------------%
%Background:
tic
%In the US, the dollar is the smallest paper currency and is worth 100
%cents. There are six different coins in circulation: 1 cent, 5 cents,
%10 cents, 25 cents, 50 cents and 100 cents (i.e., dollar coin).
%Notice that it is possible to make 2 dollars in many different ways.
%For example, the smallest number of coins needed would be 2 if you
%used only dollar coins while the largest number of coins needed would
%be 200 if you used only pennies.
%-------------------------------------------------------%
%Objective:

%Write a Matlab program that answers the following question:
%Determine how many different ways $2 can be formed using only coins. 
%The answer output by your program should be a single number.
%-------------------------------------------------------%
%Problem:
%
% Runtime is exceedingly long.
%-------------------------------------------------------%
%Our vector of options for coin choices:
%penny = 1;
%nickel = 5;
%dime = 10;
%quarter = 25; 
%50 cent piece = 50;
%dollar coin = 100;

%I would like to know how many of each coin (by themselves) will it take to generate two dollars (200 cents/coin value). I want these values because ultimately, with one type of coin, I can not surpass these values:
penny = 200/1;
nickel = 200/5;
dime = 200/10;
quarter = 200/25;
fifty = 200/50;
dollar = 200/100;

penny_vec = ones(1,penny);
nickel_vec = 5*ones(1,nickel);
dime_vec = 10*ones(1,dime);
quarter_vec = 25*ones(1,quarter);
fifty_vec = 50*ones(1,fifty);
dollar_vec = 100*ones(1,dollar); 
%All the coins we have: value times the quantity, whilst concatinating these values:
coins = [penny_vec nickel_vec dime_vec quarter_vec fifty_vec dollar_vec];

%Now, the fun part -- let us choose some combinations!
TOTAL = 0;
for k = 2:200,

combo = combine(coins, k);
combo_total = sum(combo,2);

%for j = 1:length(combo_total)
if combo_total(j) == 200
TOTAL = TOTAL + 1;
end
end
 
TOTAL
tElapsed = toc

