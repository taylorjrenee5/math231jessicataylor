%Optimization: Another combinatoric approach: Using a Generating Function
%(with symbolic packages!)

%% Jessica Taylor

clear all
close all
clc

%The coefficient for j = 200 (coefficient of (1+x^(200))) is what we need so:

syms x j

penny = symsum(x.^j,j,0,200);
nickel = symsum(x.^(5*j),j,0,40);
dime = symsum(x.^(10*j),j,0,20);
quarter = symsum(x.^(25*j),j,0,8);
fifty = symsum(x.^(50*j),j,0,4);
dollar = symsum(x.^(100*j),j,0,2);
tdollar = symsum(x.^(200*j),j,0,1); %one choice will be subtracted off because we don't actually have a two dollar coin.
%the coefficient:



combo_expr = simplifyFraction(expand(penny*nickel*dime*quarter*fifty*dollar*tdollar))
disp('Subtract 1 from the coefficient of x^(200)')
disp('The number of combinations is 2728 ')
